<?php

namespace Tweet\TweetBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class IndexControllerTest extends WebTestCase
{
    private $container;

    private $request;

    private $indexController;

    protected function setUp()
    {
        parent::setUp();
    }


    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/test');

        $this->assertContains('Hello World', $client->getResponse()->getContent());
    }

    public function testShow()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/tweets');

        $this->assertGreaterThan(0, $crawler->filter('h2')->count());
    }
}
