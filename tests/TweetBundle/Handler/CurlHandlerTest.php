<?php

namespace Tweet\TweetBundle\Tests\Handler;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tweet\TweetBundle\Handler\CurlHandler;

class CurlHandlerTest extends WebTestCase
{
    /**
     * @var CurlHandler;
     */
    private $curlHandler;

    /**
     * @var string
     */
    private $url;

    /**
     * @var array
     */
    private $header;

    protected function setUp()
    {
        $this->curlHandler = new CurlHandler();
        $this->url         = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
        $this->header      = [
            'Authorization: OAuth oauth_consumer_key="1232", oauth_nonce="1232"',
            'Expect:',
        ];
    }

    public function testExecuteRequest()
    {
        $result = $this->curlHandler->executeRequest($this->url, $this->header);

        $this->assertContains('errors', $result);
        $this->assertContains('Bad Authentication data', $result);
    }

}