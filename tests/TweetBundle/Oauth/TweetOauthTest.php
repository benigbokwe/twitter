<?php


namespace Tweet\TweetBundle\Tests\Oauth;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Tweet\TweetBundle\Oauth\TweetOauth;

class TweetOauthTest extends WebTestCase
{
    /**
     * @var string
     */
    private $url;
    /**
     * @var string
     */
    private $method;
    /**
     * @var array
     */
    private $oauth;

    /**
     * @var TweetOauth
     */
    private $tweetOauth;

    protected function setUp()
    {
        $this->url    = "https://api.twitter.com/1.1/statuses/user_timeline.json";
        $this->method = 'GET';
        $this->oauth  = [
            'oauth_consumer_key'     => '12345',
            'oauth_nonce'            => 123344,
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_token'            => '23456',
            'oauth_timestamp'        => 12344,
            'oauth_version'          => '1.0',
            'oauth_signature'        => 'hash'
        ];

        $setting = [
            'oauth_access_token'        => '1233',
            'oauth_access_token_secret' => '1234',
            'consumer_key'              => '1233',
            'consumer_secret'           => '12334',
        ];

        $this->tweetOauth = new TweetOauth($setting);
    }

    public function testBuildBaseString()
    {
        $result = $this->tweetOauth->buildBaseString($this->url,  $this->method, $this->oauth);

        $this->assertContains('GET', $result);
        $this->assertContains('oauth_consumer_key', $result);
        $this->assertContains('oauth_signature_method', $result);
        $this->assertContains('oauth_token', $result);
    }

    public function testComposeAuthorizationHeader()
    {
        $result = $this->tweetOauth->composeAuthorizationHeader($this->oauth);

        $this->assertContains('Authorization: OAuth', $result);
        $this->assertContains('oauth_consumer_key=', $result);
        $this->assertContains('oauth_timestamp=', $result);
        $this->assertContains('oauth_signature=', $result);
    }

    public function testBuildOauth()
    {
        $result = $this->tweetOauth->buildOauth($this->url, $this->method);

        $this->assertInstanceOf(TweetOauth::class, $result);
    }

    public function testGet()
    {
        $result = $this->tweetOauth->get($this->url);

        $this->assertContains('errors', $result);
    }
}