Tweeter Timeline App
========================

Simple Twitter user timeline app built with symfony 2.8.


* Requirements *
** Php 5.4 minimum requirement **

Clone this repository by typing the following command in your prefered console:

```
#!git

git clone https://benigbokwe@bitbucket.org/benigbokwe/twitter.git
```

```
#!composer


Then run composer install to install all the required dependencies
```

The internal bundle api created for this feed extraction is called **TweetBundle** located at src/Tweet/TweetBundle

** Front end **

This is to demostrate returned tweet feeds
*** http://127.0.0.1:8000/tweets ***