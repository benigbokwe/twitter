<?php

namespace Tweet\TweetBundle\Oauth;

use Tweet\TweetBundle\Handler\CurlHandler;

class TweetOauth
{
    /**
     * @var string
     */
    private $oauthAccessToken;

    /**
     * @var string
     */
    private $oauthAccessTokenSecret;

    /**
     * @var string
     */
    private $consumerKey;

    /**
     * @var string
     */
    private $requestMethod;

    /**
     * @var string
     */
    private $url;

    /**
     * @var array|null
     */
    private $oauth;

    const HMAC_SHA1 = "HMAC-SHA1";

    /**
     * @var string
     */
    private $consumerSecret;

    public function __construct(array $settings)
    {
        if (!is_array($settings) || empty($settings)) {
            throw new \Exception('Authorization header setting cannot be empty');
        }

        $expectedSettingsKeys = [
            'oauth_access_token',
            'oauth_access_token_secret',
            'consumer_key',
            'consumer_secret'
        ];

        foreach ($settings as $key => $value) {
            if (!in_array($key, $expectedSettingsKeys)) {
                throw new \Exception("Setting parameter is missing in your request");
            }
        }

        $this->oauthAccessToken       = $settings['oauth_access_token'];
        $this->oauthAccessTokenSecret = $settings['oauth_access_token_secret'];
        $this->consumerKey            = $settings['consumer_key'];
        $this->consumerSecret         = $settings['consumer_secret'];
    }

    /**
     * @param $baseURI
     * @param $method
     * @param $params
     * @return string
     */
    public function buildBaseString($baseURI, $method, $params)
    {
        $return = array();
        ksort($params);

        foreach ($params as $key => $value) {
            $return[] = rawurlencode($key) . '=' . rawurlencode($value);
        }

        return $method . "&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $return));
    }

    /**
     * @param $oauth
     * @return string
     */
    public function composeAuthorizationHeader($oauth)
    {
        $header = 'Authorization: OAuth ';
        $values = array();

        foreach ($oauth as $key => $value) {
            $values[] = "$key=\"" . rawurlencode($value) . "\"";
        }

        $header .= implode(', ', $values);

        return $header;
    }

    /**
     * @param $url
     * @param $requestMethod
     * @return $this
     */
    public function buildOauth($url, $requestMethod)
    {
        $oauth = array(
            'oauth_consumer_key'     => $this->consumerKey,
            'oauth_nonce'            => (string) mt_rand(),
            'oauth_signature_method' => self::HMAC_SHA1,
            'oauth_token'            => $this->oauthAccessToken,
            'oauth_timestamp'        => time(),
            'oauth_version'          => '1.0'
        );

        $base_info = $this->buildBaseString($url, $requestMethod, $oauth);

        $composite_key = rawurlencode($this->consumerSecret) . '&' . rawurlencode($this->oauthAccessTokenSecret);
        $oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
        $oauth['oauth_signature'] = $oauth_signature;

        $this->url           = $url;
        $this->requestMethod = $requestMethod;
        $this->oauth         = $oauth;

        return $this;
    }

    /**
     * @param $url
     * @return mixed
     * @throws \Exception
     */
    public function get($url)
    {
        $this->buildOauth($url, 'GET');
        $header =  array($this->composeAuthorizationHeader($this->oauth), 'Expect:');
        $curl = new CurlHandler();
        return $curl->executeRequest($url, $header);
    }
}
