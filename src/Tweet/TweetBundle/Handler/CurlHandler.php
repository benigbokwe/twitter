<?php

namespace Tweet\TweetBundle\Handler;

class CurlHandler
{
    /**
     * The maximum number of seconds to allow cURL functions to execute
     * @var int
     */
    private $timeout = 30;

    /**
     * The number of seconds to wait while trying to connect
     * @var int
     */
    private $connectionTimeout = 10;

    /**
     * @param $url
     * @param $header
     * @return mixed
     * @throws \Exception
     */
    public function executeRequest($url, $header)
    {
        $options = [
            CURLOPT_HTTPHEADER     => $header,
            CURLOPT_HEADER         => false,
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_CONNECTTIMEOUT => $this->connectionTimeout,
            CURLOPT_TIMEOUT        => $this->timeout
        ];

        $ch = curl_init();
        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);

        if($response === false) {
            throw new \Exception('Curl error: ' . curl_error($ch));
        }

        // close handle
        curl_close($ch);

        return $response;
    }
}