<?php

namespace Tweet\TweetBundle\Twig;

class TweetTwigExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('TweetText', array($this, 'formatTweet')),
        );
    }

    /**
     * @param $tweet
     * @return mixed
     */
    public function formatTweet($tweet)
    {
        $link      = '@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@';
        $hash      = '/(^|[\n\s])#([^\s"\t\n\r<:]*)/is';
        $mentioned = '/(^|[\n\s])@([^\s"\t\n\r<:]*)/is';

        $prettyTweet = preg_replace(
           [
               $link,
               $hash,
               $mentioned
           ],
            [
                '<a href="$1" class="link-tweet" target="_blank">$1</a>',
                '$1<a class="link-hashtag" href="https://twitter.com/search?q=%23$2&src=hash" target="_blank">#$2</a>',
                '$1<a class="link-mention" href="http://twitter.com/$2" target="_blank">@$2</a>'
            ],
            $tweet
        );

        return $prettyTweet;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'twig.extension';
    }
}
