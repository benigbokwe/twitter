<?php

namespace Tweet\TweetBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Acl\Exception\Exception;
use Tweet\TweetBundle\Oauth\TweetOauth;

class IndexController extends Controller
{
    /**
     * @Route("/test")
     */
    public function indexAction()
    {
        return $this->render('TweetBundle:Index:index.html.twig');
    }

    /**
     * @Route("/tweets", name="user_tweet")
     */
    public function showAction()
    {
        $tweetParameters = $this->container->getParameter('tweet');

        $settings = $tweetParameters['settings'];

        $url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';

        try {
            $twitter   = new TweetOauth($settings);
            $response  = $twitter->get($url);

            $message = [
                'status'   => 'ok',
                'response' => json_decode($response, true)
            ];
        } catch (\Exception $ex) {
            $message = [
                'status'  => 'error',
                'response' => $ex->getMessage()
            ];
        }

        return $this->render('TweetBundle:Index:show.html.twig', [
            'message' => $message
            ]
        );
    }
}
